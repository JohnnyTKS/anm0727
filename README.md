//	NAR README
Before using the program, plesae complete the following steps:

1. Download the ABC package. abc70930 is recommended.
2. Modify the ABC package. Before making the static library libabc.a, add the statement, "char * undef1;", into "src\base\abc\abc.h" at line 170, or use the attached file "include\abc.h" to replace "src\base\abc\abc.h".
3. For convenience, I collect all the head files (*.h) in the ABC package and put them into the attached directory "include" for inclusion. They are optional, if you have the other methods to achieve the necessary inclusion.
4. Modify the path of ABC in the makefile. 
5. Now, you could make the executable file, "nar", by typing make.

usage
>> make
>> ./nar -sarv C432.blif

-s -> if you would like to optimize the benchmark.
-a -> if you would like to perform node addition and removal.
-r -> if you would like to perfrom redundancy removal.
-v -> if you would like to verify the optimization result.

Note that the program currently works only for combinational circuits.



//	Approximate Node Merging

Approximate Node Merging 基於 NAR 的程式寫下去，所以會有 include abc 的資料夾
整個資料夾可到   git clone https://JohnnyTKS@bitbucket.org/JohnnyTKS/anm0727.git

//////////////////////////
//		  程式編譯       //
//////////////////////////
請在 NTHU ic56 編譯
主程式編譯： 						cp makefile.apx makefile ; make
測錯誤率程式編譯：				cp makefile.ver makefile ; make
江介宏老師 verify Tool編譯：		cd ../ssatABC/ ; make

//////////////////////////
//		  資料夾        //
//////////////////////////
1.	Benchmark1/	->	MCNC + IWLS2005 的 AIG.blif (所有)
2.	Benchmark/	->	MCNC + IWLS2005 的 AIG.blif (論文上的)
3.	approx/		->	Approximate Node Merging 優化後之電路結果
4.	tools/		->	Verify tool 的 code
5.	log/		->	主程式顯示之資訊

** Benchmark 前有前序 "Qaig_" 是比較對象所提供之電路，以區分 MCNC+IWLS2005 跟 比較對象 benchmark 的差異

//////////////////////////
//		  程式執行       //
//////////////////////////

1.	主程式執行

./apx [-srei123] <error rate constraint> <circuit name>

>> ./apx -sre12 5 Qaig_alu4.blif |& tee log/Qaig_alu4.log  (Our approach)
>> ./apx -sre13 5 Qaig_alu4.blif |& tee log/Qaig_alu4.log  (naive method)

-s -> Perform simplification.
-r -> Perform redundancy removal.
-e -> Estimate error rate of the resultant circuit.
-i -> Improve area when approximate logic synthesis.
-1 -> Perform approximate operation( node->Const ).
-2 -> Perform approximate operation( Node Replacement - ANM ).
-3 -> Perform approximate operation( SASIMI -> naive method ).

Note that the program currently works only for combinational circuits.

** 如果要跑論文上所有的 case ，可輸入

>> sh run_all.sh

2.	測錯誤率程式執行

有兩個版本的 verify , 使用不同方法

第一個
./tools/verify <golden.blif> <approx1.blif> <approx2.blif>

>> ./tools/verify Benchmark/Qaig_alu4.blif approx/Qaig_alu4_v1.blif approx/Qaig_alu4_v2.blif |& tee log/alu4_verify.log

第二個	(兩個 approximate circuit 預在　approx , Wu18 這兩個資料夾)
./verify <circuit.blif> <pattern number>

>> ./verify Qaig_alu4.blif 100000 |& tee log/alu4_verify.log


3.	江介宏老師 verify Tool執行：

>> ./../ssatABC/bin/abc

進入 tool 後：

UC Berkeley, ABC 1.01 (compiled Jun 29 2020 21:01:39)
abc 01> miter Benchmark/Qaig_alu4.blif approx/Qaig_alu4.blif
abc 02> bddsp -u
  > Pb_BddComputeSp() : build bdd for 0-th Po
  > Bdd construction =     0.01 sec
  > Prob computation =     0.00 sec
  > 0-th Po , prob = 4.833984e-02
abc 03> q

詳細資料可參閱 : https://github.com/nianzelee/ssatABC
